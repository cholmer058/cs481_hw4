import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(LogoApp());
}
class AnimatedLogo extends AnimatedWidget {
  // Make the Tweens static because they don't change.
  static final _opacityTween = Tween<double>(begin: 1, end: 1);
  static final _sizeTween = Tween<double>(begin: 0, end: 800);
  static final _colorTween = ColorTween(begin: Colors.yellow, end: Colors.lightBlueAccent);

  static bool switchAlign = false;

  AnimatedLogo({Key key, Animation<double> animation})
      : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final animation = listenable as Animation<double>;
    return Container(
        padding: EdgeInsets.all(10.0),
        alignment: (switchAlign ? Alignment.bottomLeft : Alignment.topRight),
        width: 200,
      child:
          Opacity(
            opacity: _opacityTween.evaluate(animation),
            child: Container(
              //margin: EdgeInsets.symmetric(vertical: 10),
              height: _sizeTween.evaluate(animation),
              width: _sizeTween.evaluate(animation),
              child: Container(
                color: _colorTween.evaluate(animation),
              ),
            ),
          )
      );

  }
}

class LogoApp extends StatefulWidget {
  _LogoAppState createState() => _LogoAppState();
}

class _LogoAppState extends State<LogoApp> with SingleTickerProviderStateMixin {
  Animation<double> animation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller =
        AnimationController(duration: const Duration(seconds: 5), vsync: this);
    animation = CurvedAnimation(parent: controller, curve: Curves.linear)
      ..addStatusListener((status) {

        if (status == AnimationStatus.completed) {
          controller.reverse();
          AnimatedLogo.switchAlign = !AnimatedLogo.switchAlign;
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
          AnimatedLogo.switchAlign = !AnimatedLogo.switchAlign;
        }
      });
    controller.forward();
  }

  @override
  Widget build(BuildContext context) => AnimatedLogo(animation: animation);

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}